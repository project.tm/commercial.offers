<?php

namespace Commercial\Offers\Config;

class Iblock {

    const OFFERS = 11;
    const PRODUCT = 12;
    const CATALOG = 2;
    const SELLER = 9;

}
