<?php

namespace Commercial\Offers\Event;

use CIBlockElement;

class Element {

    static public function OnAfterIBlockElementUpdate($arParams) {
        return self::OnAfterIBlockElementAdd($arParams);
//        preExit($arParams);
    }

    static public function OnAfterIBlockElementAdd($arParams) {
        if ($arParams['ID']) {
            $arSelect = Array("ID", "PROPERTY_NUMBER");
            $arFilter = Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "PROPERTY_NUMBER" => false, "ID" => $arParams['ID']);
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            if ($arItem = $res->Fetch()) {
                $arSelect = Array("ID", "PROPERTY_NUMBER");
                $arFilter = Array("IBLOCK_ID" => $arParams['IBLOCK_ID']);
                $res = CIBlockElement::GetList(Array('PROPERTY_NUMBER' => 'DESC'), $arFilter, false, false, $arSelect);
                if ($arItem = $res->Fetch()) {
                    CIBlockElement::SetPropertyValues($arParams['ID'], $arParams['IBLOCK_ID'], $arItem['PROPERTY_NUMBER_VALUE'] + 1, 'NUMBER');
                } else {
                    CIBlockElement::SetPropertyValues($arParams['ID'], $arParams['IBLOCK_ID'], 1, 'NUMBER');
                }
            }
        }
    }

}
